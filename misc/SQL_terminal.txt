marcosabcarvalho@devdebian:~$ mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.15 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SELECT VERSION(), CURRENT_DATE;
+-----------+--------------+
| VERSION() | CURRENT_DATE |
+-----------+--------------+
| 8.0.15    | 2019-06-06   |
+-----------+--------------+
1 row in set (0.00 sec)

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.01 sec)

mysql> SHOW TABLES;
ERROR 1046 (3D000): No database selected
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| comercial          |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec)

mysql> USE comercial
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> SHOW TABLES;
+-----------------------+
| Tables_in_comercial   |
+-----------------------+
| flyway_schema_history |
| oportunidade          |
+-----------------------+
2 rows in set (0.00 sec)

mysql> DESCRIBE comercial;
ERROR 1146 (42S02): Table 'comercial.comercial' doesn't exist
mysql> DESCRIBE oportunidade
    -> ;
+----------------+---------------+------+-----+---------+----------------+
| Field          | Type          | Null | Key | Default | Extra          |
+----------------+---------------+------+-----+---------+----------------+
| id             | bigint(20)    | NO   | PRI | NULL    | auto_increment |
| nome_prospecto | varchar(80)   | NO   |     | NULL    |                |
| descricao      | varchar(200)  | NO   |     | NULL    |                |
| valor          | decimal(10,2) | YES  |     | NULL    |                |
+----------------+---------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> 


